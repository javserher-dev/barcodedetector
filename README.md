#BarcodeDetector

**Simple Barcode Detector using Swift, AVFoundation and Alamofire frameworks, as well as some patterns like MVC (design), Singleton (in the APIClient for example) and low-coupling (using a Constants.swift file for every string literal)**

**Designed, implemented and structured always following the SOLID principles to make it as much reusable, scalable and easy to maintain as possible.***

When it detects a barcode, it reads it, looks for is related product in Buycott's database using its REST API and displays an alert asking you if you want to open a Safari WebView to search information about the product.*

**In order to make requests to the API, replace the api key in the Constants.swift file with the one from your Buycott profile. Otherwise requests may not succeed.**

**I know storing the api key in the code is not the most secure way to do it, but in this case the api key is not relevant, just to make requests and retrieve barcodes information (otherwise I would have used the Keychain). Buycott's API doesn't generate tokens for every user and the api key goes in the POST request body parameters.**

I have tried to keep it as simple as possible but as much functional and informative as possible too. It is just an example to see how it worked, I know there are a lot of things than can be added to add more functionalities but that wasn't the intention.

Please take into account that Buycott's API database doesn't work with a lot of barcodes, so in case you want to try it with a verified barcode please refer to the bottom of the code in BarcodeDetectorViewController and read the commented section.
