//
//  Constants.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//
import UIKit

typealias AlertActionCompletion = ((UIAlertAction) -> Void)

struct Constants {
    
    static let tabBarIcon = "icon"
    
    struct alert {
        struct buttonTitles {
            static let ok = "Ok"
            static let done = "Done"
            static let cancel = "Cancel"
        }
        
        struct titles {
            static let barcodeFound = "Found"
            static let error = "Error"
        }
        
        struct messages {
            static let barcodeFound = "\n\nDo you want to know more about it?"
            static let barcodeNotFound = "Product not found in the database. Please try again with a different one"
            static let cameraUsageUnauthorized = "You must allow access to the camera to use the app. Go to Settings > Privacy > Camera and check BarcodeDetector"
            static let urlLoadError = "Couldn't load url. Please try again later"
        }
    }
    
    struct apiPaths {
        static let baseUrl = "https://www.buycott.com"
        static let lookup = "/api/v4/products/lookup"
        static let search = "/api/v4/products/search"
    }
    
    static let apiKey = "w1to2Fnln71QDjlZ3PHO4dVOK3Lrnp4hW4H-LOQS"
    
    static let googleSearchUrl = "https://www.google.com/search?q="
    
    struct accesibilityLabels {
        static let scannerTabBarItem = "Scanner button"
        static let scannerLiveView = "Barcode scanner camera live view. Please put a barcode in front of the camera and wait until you hear alert"
        static let loadingScreen = "Loading screen. Please wait"
        static let webView = "Web navigator"
        static let webViewBackButton = "Back button. Exit the web view and return to the scanner"
    }
}
