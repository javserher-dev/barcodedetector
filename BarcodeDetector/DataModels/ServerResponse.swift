//
//  ServerResponse.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

struct ServerResponse: Codable {
    let success: Bool
    let statusCode, productCount: Int?
    let products: [Product]?
    
    enum CodingKeys: String, CodingKey {
        case success
        case statusCode = "status_code"
        case productCount = "product_count"
        case products
    }
}

struct Product: Codable {
    let productName: String?
    let productImageURL: String?
    let productDescription: String?
    let productAsin: String?
    let productGtin: String?
    let categoryName, categoryBrowseNodeID: String?
    let manufacturerName: String?
    let manufacturerImageURL: String?
    let brandName: String?
    let brandImageURL: String?
    let countryOfOrigin: String?
    
    enum CodingKeys: String, CodingKey {
        case productName = "product_name"
        case productImageURL = "product_image_url"
        case productDescription = "product_description"
        case productAsin = "product_asin"
        case productGtin = "product_gtin"
        case categoryName = "category_name"
        case categoryBrowseNodeID = "category_browse_node_id"
        case manufacturerName = "manufacturer_name"
        case manufacturerImageURL = "manufacturer_image_url"
        case brandName = "brand_name"
        case brandImageURL = "brand_image_url"
        case countryOfOrigin = "country_of_origin"
    }
}
