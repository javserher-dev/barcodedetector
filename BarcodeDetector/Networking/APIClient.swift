//
//  APIClient.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//
import Foundation
import Alamofire

final class APIClient: NSObject {
    
    static let shared = APIClient()
    
    //I have implemented both type of request, one using standard framework from Apple and the other one using Alamofire from Cocoapods. Just to show that I know how to work with both.
    
    func performRequest<T: Decodable> (router: APIRouter, completion: @escaping (_: T?, _: Error?) -> ()) {
        let request = try? router.asURLRequest()
        let session = URLSession(configuration: URLSessionConfiguration.default)
        if let request = request {
            session.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
                if (error != nil){
                    completion(nil, error)
                }
                else{
                    if let data = data {
                        let result = try? JSONDecoder().decode(T.self, from: data)
                        completion (result, error)
                    }
                }
            })
            .resume()
        }
    }
    
    func performAlamofireRequest <T: Decodable> (router: APIRouter, completion: @escaping (_: T?) -> ()) {
        
        Alamofire.request(router).responseData(completionHandler: { [weak self] response in
            
            //Ideally the Buycott's API should return an statusCode between 200 and 300 if it succeeded or another code if it fails, and this code would handle it (this would be the appropiate way to do it because this APIClient class should handle everything related to the server requests), but the API is not well done and I have to check it by myself in the BarcodeDetectorViewController manually. It is not my fault.
            /*guard let status = response.response?.statusCode, (200...300).contains(status) else {
                completion(nil)
                return
            }*/
            
            let result = try? JSONDecoder().decode(T.self, from: response.result.value!)
            completion (result)
        })
    }
}
