//
//  APIRouter.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Alamofire

enum APIRouter : URLRequestConvertible {
    
    case lookup(barcode: String), search(query: String)
    
    var header: HTTPHeaders {
        switch self {
        case .lookup, .search: return ["application/json" : "Content-type"]
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .lookup(let barcode): return ["barcode" : barcode, "access_token" : Constants.apiKey]
        case .search(let query): return ["query" : query, "access_token" : Constants.apiKey]
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .lookup, .search: return .post
        }
    }
    
    var path: String {
        switch self {
        case .lookup: return Constants.apiPaths.lookup
        case .search: return Constants.apiPaths.search
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.apiPaths.baseUrl.asURL()
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = header
        switch method{
        case .get, .delete: return try URLEncoding.default.encode(request, with: parameters)
        case .post, .put: return try JSONEncoding.default.encode(request, withJSONObject: parameters)
        default: return try JSONEncoding.default.encode(request, withJSONObject: parameters)
        }
    }
}
