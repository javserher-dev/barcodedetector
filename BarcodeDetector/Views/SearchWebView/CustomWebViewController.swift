//
//  CustomWebViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 11/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import WebKit

final class CustomWebViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func load(urlString: String) {
        guard let url = URL(string: urlString) else {
            debugPrint("Could not create URL instance")
            return
        }
        let request = URLRequest(url: url)
        
        if #available(iOS 12, *) {
            let webView = WKWebView(frame: self.view.bounds)
            webView.load(request)
            self.view.addSubview(webView)
        } else {
            let webView = UIWebView(frame: self.view.bounds)
            webView.loadRequest(request)
            self.view.addSubview(webView)
        }
    }
}

