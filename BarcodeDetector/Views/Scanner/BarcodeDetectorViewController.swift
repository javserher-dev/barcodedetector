//
//  BarcodeDetectorViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//
import UIKit
import AVFoundation
import WebKit

final class BarcodeDetectorViewController: UIViewController {
    
    private let session = AVCaptureSession()
    let sessionQueue = DispatchQueue(label: "sessionQueue")
    private var authorizationStatus: Bool?
    private var input: AVCaptureDeviceInput?
    private var previewLayer: AVCaptureVideoPreviewLayer!
    private var metadataOutput = AVCaptureMetadataOutput()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        if let parentView = parent?.view {
            previewLayer.frame = parentView.frame
        } else {
            previewLayer.frame = view.bounds
        }
        view.layer.addSublayer(previewLayer)
        view.isAccessibilityElement = true
        view.accessibilityLabel = Constants.accesibilityLabels.scannerLiveView
        
        checkCameraAuthorization()
        sessionQueue.async { [weak self] in
            self?.setupSession()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    private func checkCameraAuthorization() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: authorizationStatus = true
        case .notDetermined, .denied:
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] authorization in
                if authorization {
                    self?.authorizationStatus = true
                } else {
                    let alert = SimpleOKAlertViewController(title: Constants.alert.titles.error, message: Constants.alert.messages.cameraUsageUnauthorized, preferredStyle: .alert)
                    alert.setupByDefault()
                    self?.present(alert, animated: true, completion: nil)
                }
                self?.sessionQueue.resume()
            })
        default: authorizationStatus = false
        }
    }
    
    private func setupSession() {
        guard authorizationStatus == true else { return }
        session.beginConfiguration()
        session.sessionPreset = .high
        
        do {
            guard let rearCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else {
                print("Rear camera is not available.")
                session.commitConfiguration()
                return
            }
            
            let videoInput = try AVCaptureDeviceInput(device: rearCamera)
            if session.canAddInput(videoInput) {
                session.addInput(videoInput)
                session.addOutput(self.metadataOutput)
                
                setupMetadataOutput()
                
                session.commitConfiguration()
                session.startRunning()
            } else {
                print("Error while adding video device input to the session.")
                session.commitConfiguration()
                return
            }
        } catch {
            print("Error while creating video device input")
            session.commitConfiguration()
            return
        }
    }
    
    private func setupMetadataOutput() {
        self.metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        self.metadataOutput.metadataObjectTypes = [.code128, .code39, .ean13, .code93]
    }
    
    private func retrieveInformationFromApi(barcode: String) {
        let blurredVC = BlurredLoadingScreenViewController()
        blurredVC.modalPresentationStyle = .overCurrentContext
        present(blurredVC, animated: true, completion: nil)
        blurredVC.startLoading()
        
        APIClient.shared.performAlamofireRequest(router: APIRouter.lookup(barcode: barcode), completion: { [weak self] (response: ServerResponse?) in
            blurredVC.stopLoading()
            guard let response = response else { return }
            //I have to check if the request succeeded or not here. This is not the appropiate wat to do this, it should be done in APIClient (because it represents the handler of sthe erver requests), but I do this here is because it is a problem of the Buycott's API. You have the explanation of why I am doing this in APIClient.swift
            if !response.success {
                self?.presentedViewController?.dismiss(animated: true, completion: nil)
                let alert = SimpleOKAlertViewController(title: Constants.alert.titles.error, message: Constants.alert.messages.barcodeNotFound, preferredStyle: .alert)
                alert.setupAlert(completion: { action in
                    self?.dismiss(animated: true, completion: nil)
                    self?.session.startRunning()
                })
                self?.present(alert, animated: true, completion: nil)
                
            } else if let product = response.products?.first?.productName {
                let alert = TwoButtonsAlertViewController(title: Constants.alert.titles.barcodeFound, message: product + Constants.alert.messages.barcodeFound, preferredStyle: .alert)
                
                let firstButtonCompletion: AlertActionCompletion = { [weak self] (action) in
                    let webViewVC = CustomWebViewController()
                    webViewVC.load(urlString: GoogleSearchQueryFormatter().formatToGoogleQuery(text: product))
                    self?.navigationController?.navigationBar.isHidden = false
                    self?.navigationController?.pushViewController(webViewVC, animated: true)
                    self?.session.startRunning()
                }
                
                let secondButtonCompletion: AlertActionCompletion = { [weak self] action in
                    self?.dismiss(animated: true, completion: nil)
                    self?.session.startRunning()
                }
                
                alert.setupAlert(firstButtonCompletion: firstButtonCompletion, secondButtonCompletion: secondButtonCompletion)
                self?.present(alert, animated: true, completion: nil)
            }
        })
    }
    
}

extension BarcodeDetectorViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        session.stopRunning()
        if metadataObjects.count > 0 {
            guard let barcode = metadataObjects.first as? AVMetadataMachineReadableCodeObject else { return }
            retrieveInformationFromApi(barcode: barcode.stringValue!)
            
//            //In case the Buycott API doesn't have information in its database for the barcode scanned by the camera, this example will work. Just comment the above call to retrieveInformationFromApi and uncomment the following 2 lines
//            let code = "888462323772"
//            retrieveInformationFromApi(barcode: code)
        }
    }
}
