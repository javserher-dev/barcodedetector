//
//  BlurredLoadingScreenViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class BlurredLoadingScreenViewController: UIViewController {
    
    private var loadingSpinner = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setStyle()
    }
    
    func setupView() {
        let blurr = UIBlurEffect(style: .light)
        let blurrView = UIVisualEffectView(effect: blurr)
        blurrView.frame = view.bounds
        view.addSubview(blurrView)
        
        loadingSpinner.frame = CGRect(x: 0, y: 0, width: 75, height: 75)
        loadingSpinner.center = view.center
        view.addSubview(loadingSpinner)
    }
    
    private func setStyle() {
        loadingSpinner.color = .black
    }
    
    func startLoading() {
        loadingSpinner.startAnimating()
    }
    
    func stopLoading() {
        loadingSpinner.stopAnimating()
        dismiss(animated: true, completion: nil)
    }
}

