//
//  SimpleOKAlertViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class SimpleOKAlertViewController: UIAlertController {
    
    func setupAlert(completion: @escaping AlertActionCompletion) {
        self.addAction(UIAlertAction(title: Constants.alert.buttonTitles.ok, style: .default, handler: completion))
    }
    
    func setupByDefault() {
        self.addAction(UIAlertAction(title: Constants.alert.buttonTitles.ok, style: .default, handler: { [weak self] action in
            self?.dismiss(animated: true, completion: nil)
        }))
    }
}
