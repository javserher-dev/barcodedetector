//
//  TwoButtonsAlertViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class TwoButtonsAlertViewController: UIAlertController {
    
    func setupAlert(firstButtonCompletion: @escaping AlertActionCompletion, secondButtonCompletion: @escaping AlertActionCompletion) {
        self.addAction(UIAlertAction(title: Constants.alert.buttonTitles.ok, style: .default, handler: firstButtonCompletion))
        self.addAction(UIAlertAction(title: Constants.alert.buttonTitles.cancel, style: .default, handler: secondButtonCompletion))
    }
}
