//
//  TabBarViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 10/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewControllers()
        setStyle()
    }
    
    private func setupViewControllers() {
        let firstNavigationVC = UINavigationController(rootViewController: BarcodeDetectorViewController())
        firstNavigationVC.tabBarItem = UITabBarItem(title: "Scanner", image: UIImage(named: Constants.tabBarIcon), selectedImage: nil)
        
        //In case there are more view controllers to show, we would instantiate them here in order to show more icons in the TabBar. One NavigationController for each View Controller we want to show. For now we just have the main view controller of the scanner
        
        viewControllers = [firstNavigationVC]
    }
    
    private func setStyle() {
        tabBar.tintColor = .blue
    }
}
